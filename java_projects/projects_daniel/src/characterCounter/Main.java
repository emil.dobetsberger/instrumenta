package characterCounter;

public class Main {
	public static String[] checkParamters(String[] parameters) {
		
		// check empty text
		if (parameters.length == 0 || parameters[0] == null) {
			return new String[]{"No characters can be counted, if there is no text."};
		}
		
		// check length
		int length = parameters.length;
		
		int nParams = CharacterCounter.AMOUNT_OF_PARAMETERS;
		
		if (length != nParams) {
			switch (length) {
				case 0:
					return new String[]{String.format("While %d parameters are expected, none was given.", nParams)};
					
				case 1:
					return new String[]{String.format("While %d parameters are expected, only one was given.", nParams)};
					
				default:
					return new String[]{String.format("While %d parameters are expected, %d were given.", nParams, length)};
			}
		}
		
		// check booleans
		for (int i = 1; i <= 5; i++) {			
			String param = parameters[i];

			if (param.equals(new String("0"))) parameters[i] = "false";
			else if (param.equals(new String("1"))) parameters[i] = "true";
			else return new String[]{String.format("While the %d. parameter is expected to be 0 or 1, it was %s.", i, param)};
		}
		
		// check mutated vowels
		String param = parameters[6];
		
		if (!param.equals(new String("0")) &&
				!param.equals(new String("1")) &&
				!param.equals(new String("2"))) {
			return new String[]{String.format("While the 6. parameter is expected to be 0, 1 or 2, it was %s.", param)};
		}
		
		return parameters;
	}
	
	public static void main(String[] args) {
		
		/*	
			This is how the arguments are expected and computed:
			args[0]		input text				[ String ]
			args[1]		ignore new line			[ 0 | 1 ]
			args[2]		ignore carriage return	[ 0 | 1 ]
			args[3]		ignore space			[ 0 | 1 ]
			args[4]		ignore case				[ 0 | 1 ]
			args[5]		ignore special chars	[ 0 | 1 ]
			args[6]		change mutated vowel 	[ 0 | 1 | 2 ]
			
			For args 1 - 5:
				0 --> do not ...
				1 --> do ...
			
			For arg 6:
				0 --> (ä -> ä)
				1 --> (ä -> a)
				2 --> (ä -> ae)
		*/
		
		// Check for too many/little or illegal arguments.
		String error = (checkParamters(args))[0];
		
		// Print the error and exit the program if there were no arguments,
		// or if an illegal argument was found while checking.
		if (args.length == 0 || error != args[0]) {
			System.out.print(error);
			return;
		}
		
		// Create new character counter.
		final CharacterCounter characterCounter = new CharacterCounter(args[0]);
		
		// Apply settings from args[] to the character Counter.
		characterCounter.trySetIgnoreNewLine(Boolean.valueOf(args[1]));
		characterCounter.trySetIgnoreCarriageReturn(Boolean.valueOf(args[2]));
		characterCounter.trySetIgnoreSpace(Boolean.valueOf(args[3]));
		characterCounter.trySetIgnoreCase(Boolean.valueOf(args[4]));
		characterCounter.trySetIgnoreSpecialCharacters(Boolean.valueOf(args[5]));
		characterCounter.trySetMutatedVowels(Integer.valueOf(args[6]));
		
		// Print the visual representation of the character Counter.
		System.out.println(characterCounter);
	}
}