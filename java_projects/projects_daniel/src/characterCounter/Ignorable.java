package characterCounter;

enum Ignorable {
	NewLine,
	CarriageReturn,
	Space,
	Case,
	SpecialChars;
}