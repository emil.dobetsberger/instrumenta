package characterCounter;

import java.util.Map.Entry;
import java.util.TreeMap;

public class CharacterCounter {
	// --------------- VARIABLES ---------------
	// public static ---------------------------
	final public static int AMOUNT_OF_PARAMETERS = 7;
	
	// private ---------------------------------
	final private String NEW_LINE;
	final private short NEW_LINE_CHAR;
	final private short CARRIAGE_RETURN_CHAR;
	final private short SPACE_CHAR;
	
	final private TreeMap<Short, Integer> normalChars;
	final private TreeMap<Short, Integer> specialChars;
	private InnerCharacterCounter changedChars;
	private int charsAmount;
	private int differentChars;	
	private int normalCharsAmount;
	private int specialCharsAmount;
	
	private boolean ignoreNewLine;
	private boolean ignoreCarriageReturn;
	private boolean ignoreSpace;
	private boolean ignoreCase;
	private boolean ignoreSpecialChars;
	private int mutatedVowelsType;
	
	// ------------- CONSTRUCTORS --------------
	public CharacterCounter(String text) {
		// initialise default values
		// public
		this.ignoreNewLine = false;
		this.ignoreCarriageReturn = false;
		this.ignoreSpace = false;
		this.ignoreCase = false;
		this.ignoreSpecialChars = false;
		this.mutatedVowelsType = 0;
		
		// private
		this.NEW_LINE = "\n";
		this.NEW_LINE_CHAR = 10;
		this.CARRIAGE_RETURN_CHAR = 13;
		this.SPACE_CHAR = 32;
		
		this.normalChars = new TreeMap<Short, Integer>();
		this.specialChars = new TreeMap<Short, Integer>();
		this.charsAmount = 0;
		this.differentChars = 0;
		this.normalCharsAmount = 0;
		this.specialCharsAmount = 0;
		
		// fill map with characters as shorts
		// Due to the specifications and rules of the GUI, we can safely assume,
		// that '\\' and '\n' are the only possible escape sequences in the text.
		boolean prevSlash = false;
		
		for (char c : text.toCharArray()) {
			if (c == '\\') {
				if(prevSlash) {
					addKey((short) '\\');			// If current char is '\' and the previous was '\', add '\'.
					prevSlash = false;
				} else {
					prevSlash = true;				// If current char is '\' but the previous wasn't '\', wait for the next char.
				}
			} else {
				if (prevSlash) {		// If current char is 'n' and the previous was '\', .
					if (c == 'n') {
						addKey(NEW_LINE_CHAR);
						prevSlash = false;
					} else {
						System.out.format("This text can't be properly processed, as '\\%c' must not be in the given text.", c);
						System.exit(0);
					}
				} else {							// If current char isn't '\' and the previous wasn't '\', normally add the char.
					addKey((short) c);
				}
			}
		}
		
		changedChars = new InnerCharacterCounter();
	}
	
	// ---------------- METHODS ----------------
	// public ----------------------------------
	public boolean trySetIgnoreNewLine(boolean doIgnore) {
		return trySet(doIgnore, Ignorable.NewLine);
	}
	
	public boolean trySetIgnoreCarriageReturn(boolean doIgnore) {
		return trySet(doIgnore, Ignorable.CarriageReturn);
	}
	
	public boolean trySetIgnoreSpace(boolean doIgnore) {
		return trySet(doIgnore, Ignorable.Space);
	}
	
	public boolean trySetIgnoreCase(boolean doIgnore) {
		return trySet(doIgnore, Ignorable.Case);
	}
	
	public boolean trySetIgnoreSpecialCharacters(boolean doIgnore) {
		return trySet(doIgnore, Ignorable.SpecialChars);
	}
	
	public boolean trySetMutatedVowels(int type) {
		if (type < 0 || 2 < type) return false;				// Only type 0, 1 or 1 can be assigned.
		if (mutatedVowelsType == type) return false;		// If it was tried to reassign the same value, do nothing.
		
		mutatedVowelsType = type;
		changeMutatedVowel();
		return true;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(changedChars.printOverview());
		sb.append(changedChars.printNormalCharacters());
		sb.append(changedChars.printSpecialCharacters());
		
		return sb.toString();
	}
	
	// private ---------------------------------
	private void addKey(short key) {
		int value = 0;
		TreeMap<Short, Integer> map;
		
		if (isNormalCharacter(key)) {
			map = normalChars;
			normalCharsAmount++;
		} else {
			map = specialChars;
			specialCharsAmount++;
		}
		
		try {
			value = map.putIfAbsent(key, 1);		// Adds the char with starting value of 1 if it's a new char.
			map.put(key, value + 1);				// Raises the counter by 1, if the char is already in the map.
		} catch (NullPointerException e) {
			differentChars++;						// Raises the 'differentChars' counter if the char was not in the map.
		} finally {
			charsAmount++;
		}
	}
	
	private boolean trySet(final boolean doIgnore, final Ignorable charToIgnore) {		// Returns a boolean if the value was changed successfully.
		boolean curIgnoreVal;
		
		switch (charToIgnore) {
			case NewLine:
				curIgnoreVal = ignoreNewLine;
				break;
			
			case CarriageReturn:
				curIgnoreVal = ignoreCarriageReturn;
				break;
			
			case Space:
				curIgnoreVal = ignoreSpace;
				break;
			
			case Case:
				curIgnoreVal = ignoreCase;
				break;
			
			case SpecialChars:
				curIgnoreVal = ignoreSpecialChars;
				break;
				
			default:
				return false;
		}
		
		if (curIgnoreVal == doIgnore) return false;		// If it was tried to reassign the same value, do nothing.
		
		// Only apply changes, if a different value is assigned
		switch (charToIgnore) {
			case NewLine:
				ignoreNewLine = doIgnore;
				ignoreCharacter(ignoreNewLine, NEW_LINE_CHAR);
				return true;
			
			case CarriageReturn:
				ignoreCarriageReturn = doIgnore;
				ignoreCharacter(ignoreCarriageReturn, CARRIAGE_RETURN_CHAR);
				return true;
			
			case Space:
				ignoreSpace = doIgnore;
				ignoreCharacter(ignoreSpace, SPACE_CHAR);
				return true;
			
			case Case:
				ignoreCase = doIgnore;
				ignoreCase();
				return true;
			
			case SpecialChars:
				ignoreSpecialChars = doIgnore;
				ignoreSpecialCharacters();
				return true;

			default:
				return false;
		}
	}
	
	private void ignoreCharacter(boolean doIgnore, final short character) {
		changedChars.ignoreCharacter(doIgnore, character);		
	}
	
	private void ignoreCase() {
		changedChars.ignoreCase();
	}
	
	private void ignoreSpecialCharacters() {
		changedChars.ignoreSpecialCharacters();
	}
	
	private void changeMutatedVowel() {
		changedChars.changeMutatedVowels();
	}

	private boolean isNormalCharacter(short character) {
		return ((65 <= character && character <= 90)						// capital letters
			|| (character == 196 || character == 214 || character == 220	// Ä, Ö, Ü
			||  character == 223 || isLowerCase(character)));				// ß + lower case
	}
	
	private boolean isLowerCase(short character) {
		return ((97 <= character && character <= 122)							// small letters
				|| character == 228 || character == 246 || character == 252);	// ä, ö, ü
	}
	
	// -------------- INNER CLASS --------------	
	private class InnerCharacterCounter {
		final private TreeMap<Short, Integer> normalChars;
		final private TreeMap<Short, Integer> specialChars;
		private int charsAmount;
		private int differentChars;	
		private int normalCharsAmount;
		private int specialCharsAmount;
		
		@SuppressWarnings("unchecked")
		private InnerCharacterCounter() {
			this.normalChars = (TreeMap<Short, Integer>) CharacterCounter.this.normalChars.clone();
			this.specialChars = (TreeMap<Short, Integer>) CharacterCounter.this.specialChars.clone();
			this.charsAmount = CharacterCounter.this.charsAmount;
			this.differentChars = CharacterCounter.this.differentChars;
			this.normalCharsAmount = CharacterCounter.this.normalCharsAmount;
			this.specialCharsAmount = CharacterCounter.this.specialCharsAmount;
		}
		
		private void ignoreCharacter(boolean doIgnore, short character) {
			try {
				
				// Remember the value of the character in the original map.
				int charAmount = CharacterCounter.this.specialChars.get(character);
				
				if (doIgnore) {									// lower and remove
					this.differentChars--;
					this.charsAmount -= charAmount;
					this.specialCharsAmount -= charAmount;
					this.specialChars.remove(character);
				} else {										// raise and put
					this.differentChars++;
					this.charsAmount += charAmount;
					this.specialCharsAmount += charAmount;
					this.specialChars.put(character, charAmount);
				}
			} catch (NullPointerException e) {		// Character is not in the map and can't be ignored.
				return;
			}
		}
		
		@SuppressWarnings("unchecked")
		private void ignoreCase() {
			if (ignoreCase) {
				
				// Only access the currently needed part of the map, which is 'a' to 'z'.
				for (Short key: ((TreeMap<Short, Integer>) this.normalChars
						.clone())
						.subMap((short) 'a', (short) ('z' + 1))
						.keySet()) {
					
					ignoreCaseHelperDoIgnore(key);
				}
				
				// Access the three lower case mutated vowels 'ä', 'ö' and 'ü'.
				ignoreCaseHelperDoIgnore((short) 'ä');
				ignoreCaseHelperDoIgnore((short) 'ö');
				ignoreCaseHelperDoIgnore((short) 'ü');
			} else {
				
				// Only access the currently needed part of the map, which is 'A' to 'Z'.
				for (Short key: ((TreeMap<Short, Integer>) this.normalChars
						.clone())
						.subMap((short) 'A', (short) ('Z' + 1))
						.keySet()) {
					
					ignoreCaseHelperDoNotIgnore(key);
				}
				
				// Access the three lower case mutated vowels 'ä', 'ö' and 'ü'.
				ignoreCaseHelperDoNotIgnore((short) 'Ä');
				ignoreCaseHelperDoNotIgnore((short) 'Ö');
				ignoreCaseHelperDoNotIgnore((short) 'Ü');
			}
		}
		
		private void ignoreCaseHelperDoIgnore(short key) {
			try {
				// If the key does not exist, catch the NullPointerException.
				int valOfSmal = this.normalChars.get(key);
				
				// With the offset 32 the corresponding capital letter is accessed.
				short keyOfCapital = (short) (key - 32);
				
				int putVal;
				
				try {
					// If the key does not exist, catch the NullPointerException.
					int valOfCapital = this.normalChars.get(keyOfCapital);
					
					// If no exception was thrown, the corresponding capital letter exists.
					this.differentChars--;
					putVal = valOfCapital + valOfSmal;
				} catch (NullPointerException e) {
					// If an exception was thrown, the corresponding capital letter does not exist.
					putVal = valOfSmal;
				}
				
				this.normalChars.put(keyOfCapital, putVal);
				this.normalChars.remove(key);
				
			} catch (NullPointerException e) {}		// The lower case Character is not in the map and can't be ignored.
		}
		
		private void ignoreCaseHelperDoNotIgnore(short key) {
			try {
				// If the key does not exist, catch the NullPointerException.
				int valOfCapital = this.normalChars.get(key);

				// With the offset 32 the corresponding lower letter is accessed.
				short keyOfLower = (short) (key + 32);
				
				try {
					// If the key does not exist, catch the NullPointerException.
					int oldValOfLower = CharacterCounter.this.normalChars.get(keyOfLower);
					
					// Set the amount of the corresponding lower case letter to the original amount.
					this.normalChars.put(keyOfLower, oldValOfLower);

					// Reduce the amount of the capital letter, by the original amount of the lower case letter.
					if (valOfCapital - oldValOfLower == 0) {
						this.normalChars.remove(key);
					} else {
						this.normalChars.put(key, valOfCapital - oldValOfLower);						
						this.differentChars++;
					}
				} catch (NullPointerException e) {
					// As no lower case character is in the original map, nothing was changed.
					return;
				}
			} catch (NullPointerException e) {
				// As the corresponding upper case Character is not in the map,
				// there was no corresponding lower or upper case letter in the original one.
				return;
			}
		}
		
		private void ignoreSpecialCharacters() {
			
			// Don't remove or put any values, simply adjust, as the printSpecialCharacters() handles this.
			if (ignoreSpecialChars) {
				for (Entry<Short, Integer> entry : specialChars.entrySet()) {
					this.differentChars--;
					this.charsAmount -= entry.getValue();
				}
			} else {
				for (Entry<Short, Integer> entry : specialChars.entrySet()) {
					this.differentChars++;
					this.charsAmount += entry.getValue();
				}			
			}
		}
		
		private void changeMutatedVowels() {
			switch (mutatedVowelsType) {
			case 1:
				changeMutatedVowelsHelperOne();
				return;
			case 2:
				changeMutatedVowelsHelperTwo();
				return;
			default:		// Case '0' (and all values except '1' or '2' which is technically impossible.)
				return;
			}
		}
		
		private void changeMutatedVowelsHelperOne() {
			changeMutatedVowelsHelperOne((short) 'Ä');
			changeMutatedVowelsHelperOne((short) 'Ö');
			changeMutatedVowelsHelperOne((short) 'Ü');
			changeMutatedVowelsHelperOne((short) 'ä');
			changeMutatedVowelsHelperOne((short) 'ö');
			changeMutatedVowelsHelperOne((short) 'ü');
		}
		
		private void changeMutatedVowelsHelperOne(short key) {
			try {
				// If the key does not exist, catch the NullPointerException.
				int valOfMutatedVowel = this.normalChars.get(key);
				
				// With the offset 131 (for 'Ä' and 'ä') or 135 (for the others) the corresponding capital letter is accessed.
				short keyOfNormal;
				
				if (key == (short) 'Ä' || key == (short) 'ä') keyOfNormal = (short) (key - 131);
				else keyOfNormal = (short) (key - 135);
				
				int putVal;
				
				try {
					// If the key does not exist, catch the NullPointerException.
					int valOfNormal = this.normalChars.get(keyOfNormal);
					
					// If no exception was thrown, the corresponding capital letter exists.
					this.differentChars--;
					putVal = valOfNormal + valOfMutatedVowel;
				} catch (NullPointerException e) {
					// If an exception was thrown, the corresponding capital letter does not exist.
					putVal = valOfMutatedVowel;
				}
				
				this.normalChars.put(keyOfNormal, putVal);
				this.normalChars.remove(key);
				
				
			} catch (NullPointerException e) {} // The Character is not in the map so nothing can be modified.
		}
		
		private void changeMutatedVowelsHelperTwo() {
			changeMutatedVowelsHelperTwo((short) 'Ä');
			changeMutatedVowelsHelperTwo((short) 'Ö');
			changeMutatedVowelsHelperTwo((short) 'Ü');
			changeMutatedVowelsHelperTwo((short) 'ä');
			changeMutatedVowelsHelperTwo((short) 'ö');
			changeMutatedVowelsHelperTwo((short) 'ü');
		}
		
		private void changeMutatedVowelsHelperTwo(short key) {
			try {
				// If the key does not exist, catch the NullPointerException.
				int valOfMutatedVowel = this.normalChars.get(key);
				
				// With the offset 131 (for 'Ä' and 'ä') or 135 (for the others) the corresponding capital letter is accessed.
				short keyOfNormal;
				
				if (key == (short) 'Ä' || key == (short) 'ä') keyOfNormal = (short) (key - 131);
				else keyOfNormal = (short) (key - 135);
				
				int putVal;
				
				try {
					// If the key does not exist, catch the NullPointerException.
					int valOfNormal = this.normalChars.get(keyOfNormal);
					
					// If no exception was thrown, the corresponding capital letter exists.
					this.differentChars--;
					putVal = valOfNormal + valOfMutatedVowel;
				} catch (NullPointerException e) {
					// If an exception was thrown, the corresponding capital letter does not exist.
					putVal = valOfMutatedVowel;
				}
				
				this.normalChars.put(keyOfNormal, putVal);
				
				// Change 'E' or 'e'				
				short propperCaseE = isLowerCase(key) ? (short) 'e' : (short) 'E';

				try {
					int valOfLetterE = this.normalChars.get(propperCaseE);
					
					// If no exception was thrown, the letter 'E' or 'e' exists.
					putVal = valOfLetterE + valOfMutatedVowel;					
				} catch (NullPointerException e) {
					
					// If an exception was thrown, the letter 'E' or 'e' does not exist.
					this.differentChars++;
					putVal = valOfMutatedVowel;					
				}
				
				this.normalChars.put(propperCaseE, putVal);
				this.normalChars.remove(key);
			} catch (NullPointerException e) {} // The Character is not in the map so nothing can be modified.
		}
		
		public String toString() {
			StringBuilder sb = new StringBuilder();
			
			sb.append(printOverview());
			sb.append(printNormalCharacters());
			sb.append(printSpecialCharacters());
			
			return sb.toString();
		}
		
		private StringBuilder printOverview() {
			StringBuilder sb = new StringBuilder();
			
			sb.append(NEW_LINE);
			sb.append("Overview:");
			sb.append(NEW_LINE);
			sb.append("character amount: " + this.charsAmount);
			sb.append(NEW_LINE);
			sb.append("different chars: " + this.differentChars);
			sb.append(NEW_LINE);
			
			return sb;	
		}
		
		private StringBuilder printNormalCharacters() {
			StringBuilder sb = new StringBuilder();
			
			if (this.normalCharsAmount <= 0) return sb;
			
			sb.append(NEW_LINE);
			sb.append(String.format("Normal Characters: [%d]", this.normalCharsAmount));
			sb.append(NEW_LINE);
			
			for (Entry<Short, Integer> entry : this.normalChars.entrySet()) {
				short key = entry.getKey();
				int val = entry.getValue();

				sb.append((char) key + ": " + val);
				sb.append(NEW_LINE);
			}
			
			return sb;	
		}
		
		private StringBuilder printSpecialCharacters() {
			StringBuilder sb = new StringBuilder();
			
			if (this.specialCharsAmount == 0 || ignoreSpecialChars) return sb;
			
			sb.append(NEW_LINE);
			sb.append(String.format("Special Characters: [%d]", this.specialCharsAmount));
			sb.append(NEW_LINE);
			
			for (Entry<Short, Integer> entry : this.specialChars.entrySet()) {
				short key = entry.getKey();
				int val = entry.getValue();
				
				if (key == NEW_LINE_CHAR) {
					if (!ignoreNewLine) {
						sb.append(("[New Line]" + ": " + val));
						sb.append(NEW_LINE);
					}
				} else if (key == CARRIAGE_RETURN_CHAR) {
					if (!ignoreCarriageReturn) {
						sb.append(("[Carriage Return]" + ": " + val));
						sb.append(NEW_LINE);
					}
				} else if (key == SPACE_CHAR) {
					if (!ignoreSpace) {
						sb.append(("[Space]" + ": " + val));
						sb.append(NEW_LINE);
					}
				} else {
					sb.append((char) key + ": " + val);
					sb.append(NEW_LINE);
				}
			}
			
			return sb;	
		}
	}	
}