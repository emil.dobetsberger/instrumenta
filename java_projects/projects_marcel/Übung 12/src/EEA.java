public class EEA {

    private int a, b;

    public EEA(int a, int b) {

        if (a <= 0 || b <= 0) throw new IllegalArgumentException("p and q must be positive integers");

        this.a = a;
        this.b = b;

    }

    //  gibt Array [gcd, p, q] wieder mit --> gcd = greatestCommonDivider(a, b) und gcd = p * a + q * b
    private static int[] greatestCommonDivider(int x, int y) {

        if (y == 0) {
            int[] myIntArray = new int[] {x, 1, 0};
            return myIntArray;
        }
        int[] result = greatestCommonDivider(y, x % y);
        int d = result[0];
        int q = result[1] - (x / y) * result[2];
        int p = result[2];
        int[] myIntArray = new int[] {d, p, q};
        return myIntArray;
    }

    @Override
    public String toString() {

        return String.format("%d = %d * %d + %d * %d", greatestCommonDivider(a, b)[0], greatestCommonDivider(a, b)[1], a, greatestCommonDivider(a, b)[2], b);

    }
}