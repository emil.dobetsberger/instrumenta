import Techniques.*;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class Main {

    public static void main(String args[]) {
        try {
            //get the information what we want to do, as example encode/Caesar/password
            String context = args[0];

            //get the text we want to encode or decode
            String text = args[1];
            byte[] bytes=text.getBytes(StandardCharsets.US_ASCII);
            text=new String(Base64.getDecoder().decode(bytes), StandardCharsets.UTF_8);

            //split the information
            List<Integer> positions = new ArrayList<>();
            for (int i = 0; i < context.length(); i++) {
                if (context.charAt(i) == '/') positions.add(i);
            }
            Object[] array = positions.toArray();
            int[] posArray = new int[array.length];
            for (int i = 0; i < array.length; i++) {
                posArray[i] = (int) array[i];
            }

            String newText;

            //encode or decode the text with the right method
            if (posArray.length < 1 || posArray.length > 2) {
                throw new IllegalArgumentException();
            } else {
                if (posArray.length == 1) {
                    String first = context.substring(0, posArray[0]);
                    String second = context.substring(posArray[0] + 1);
                    switch (first) {
                        case "Binary":
                            switch (second) {
                                case "encode":
                                    newText=new Binary().encode(text);
                                    break;
                                case "decode":
                                    newText=new Binary().decode(text);
                                    break;
                                default:
                                    throw new IllegalArgumentException();
                            }
                            break;
                        case "Fence":
                            switch (second) {
                                case "encode":
                                    newText=new Fence().encode(text);
                                    break;
                                case "decode":
                                    newText=new Fence().decode(text);
                                    break;
                                default:
                                    throw new IllegalArgumentException();
                            }
                            break;
                        case "Gitter":
                            switch (second) {
                                case "encode":
                                    newText=new Gitter().encode(text);
                                    break;
                                case "decode":
                                    newText=new Gitter().decode(text);
                                    break;
                                default:
                                    throw new IllegalArgumentException();
                            }
                            break;
                        case "Reflect":
                            switch (second) {
                                case "encode":
                                    newText=new Reflect().encode(text);
                                    break;
                                case "decode":
                                    newText=new Reflect().decode(text);
                                    break;
                                default:
                                    throw new IllegalArgumentException();
                              }
                            break;
                        default:
                            throw new IllegalArgumentException();
                    }
                } else {
                    String first = context.substring(0, posArray[0]);
                    String second = context.substring(posArray[0] + 1, posArray[1]);
                    String third = context.substring(posArray[1] + 1);
                    switch (first) {
                        case "Caesar":
                            switch (second) {
                                case "encode":
                                    newText=new Caesar().encode(text, third);
                                    break;
                                case "decode":
                                    newText=new Caesar().decode(text, third);
                                    break;
                                default:
                                    throw new IllegalArgumentException();
                            }
                            break;
                        case "LetterInsert":
                            int third2 = Integer.valueOf(third);
                            switch (second) {
                                case "encode":
                                    newText=new LetterInsert().encode(text, third2);
                                    break;
                                case "decode":
                                    newText=new LetterInsert().decode(text, third2);
                                    break;
                                default:
                                    throw new IllegalArgumentException();
                            }
                            break;
                        case "Rot":
                            int third3 = Integer.valueOf(third);
                            switch (second) {
                                case "encode":
                                    newText=new Rot().encode(text, third3);
                                    break;
                                case "decode":
                                    newText=new Rot().decode(text, third3);
                                    break;
                                default:
                                    throw new IllegalArgumentException();
                            }
                            break;
                        case "Vigenere":
                            switch (second) {
                                case "encode":
                                    newText=new Vigenere().encode(text, third);
                                    break;
                                case "decode":
                                    newText=new Vigenere().decode(text, third);
                                    break;
                                default:
                                    throw new IllegalArgumentException();
                            }
                            break;
                        default:
                            throw new IllegalArgumentException();
                    }
                }
                bytes=newText.getBytes(StandardCharsets.UTF_8);
                newText=new String(Base64.getEncoder().encode(bytes), StandardCharsets.US_ASCII);
                System.out.println(newText);
            }
        }catch (Caesar.MyException e){
            System.out.println(e.getMessage());
        }catch(IllegalArgumentException p){
            System.out.println("Falsche Eingabe");
        }

    }
}
