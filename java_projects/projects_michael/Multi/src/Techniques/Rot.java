package Techniques;

public class Rot {

    /**
     *  If a number is negative we add the mod to make it positive again.
     *
     * @param number The negative number
     * @param mod
     * @return The positive number in the same modulo class
     */
    public static int modNegative(int number, int mod){
        while (number <0){
            number+=mod;
        }
        return number;
    }

    /**
     * To make it easier to encode this matrix gives us back a matrix to shows us what letter represents another one
     *
     * @param times How many positions we want to move the letters
     * @return A matrix that shows us what letter represents another
     */
    public static char[] move(int times){
        times= times%26;
        char[] matrix=new char[26];
        for(int i=0; i<matrix.length;i++){
            matrix[i]=(char)('a'+modNegative(i-times, 26));
        }
        return matrix;
    }

    /**
     * Moves the letters in the alphabet to a new position
     *
     * @param text The text we want to encode
     * @param times How many positions we want to move the letter in the alphabet
     * @return The encoded text
     */
    public String encode(String text, int times){
        StringBuilder encoded = new StringBuilder();
            char[] matrix = move(times);
        for(int i=0;i<text.length(); i++){
            if(text.charAt(i)>='a'&&text.charAt(i)<='z'){
                encoded.append(matrix[text.charAt(i)-'a']);
            }else if(text.charAt(i)>='A'&&text.charAt(i)<='Z'){
                encoded.append(Character.toUpperCase(matrix[text.charAt(i)-'A']));
            }else{
                encoded.append(text.charAt(i));
            }
        }
        return encoded.toString();
    }

    /**
     * We reverse the things we did with the encode method
     *
     * @param text The decoded text
     * @param times How many positions we moved the letters in the alphabet
     * @return The decoded Text
     */
    public String decode(String text, int times){
        StringBuilder encoded = new StringBuilder();
        char[] matrix = move(times);
        for(int i=0;i<text.length(); i++){
            if(text.charAt(i)>='a'&&text.charAt(i)<='z'){
                for(int j=0; j<matrix.length;j++){
                    if(matrix[j]==text.charAt(i)){
                        encoded.append(((char)('a'+j)));
                        break;
                    }
                }
            }else if(text.charAt(i)>='A'&&text.charAt(i)<='Z'){
                for(int j=0; j<matrix.length;j++){
                    if(matrix[j]==Character.toLowerCase(text.charAt(i))){
                        encoded.append(((char)('A'+j)));
                        break;
                    }
                }
            }else{
                encoded.append(text.charAt(i));
            }
        }
        return encoded.toString();
    }

}
