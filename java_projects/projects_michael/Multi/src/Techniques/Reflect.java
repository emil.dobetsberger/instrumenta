package Techniques;

import java.util.ArrayList;

public class Reflect {

    /**
     * Filters out the words in the text because with the encode method we will only encode words.
     *
     * @param text The text we want to encode
     * @return A list with the words we have in the text
     */
    public static Object[] textAsArray(String text) {
        ArrayList<String> words = new ArrayList<String>();
        int i = 0;
        int last = text.length() - 1;
        while (i <= last) {
            int startPoint = i;
            while (i <= last && !Character.isLetter(text.charAt(i))) i++;
            if (i > startPoint) words.add(text.substring(startPoint, i));
            int startPoint2 = i;
            while (i <= last && Character.isLetter(text.charAt(i))) i++;
            if (i > startPoint2) words.add(text.substring(startPoint2, i));
        }
        return words.toArray();
    }

    /**
     * Recursive method that reflects a single word
     *
     * @param word The word we want to reflect
     * @return The reflected word
     */
    public static String reflect(String word) {
        if (word.length() <= 1) return word;
        return word.charAt(word.length() - 1) + reflect(word.substring(1, word.length() - 1)) + word.charAt(0);
    }

    /**
     * Reflects every word in the text by using the reflect method.
     *
     * @param text The word we want to encode
     * @return The encoded word
     */
    public String encode(String text) {
        StringBuilder encoded = new StringBuilder();
        Object[] textArray = textAsArray(text);
        for (int i = 0; i < textArray.length; i++) {
            if(!Character.isLetter(((String)textArray[i]).charAt(0))){
                encoded.append((String) textArray[i]);
            }else {
                encoded.append(reflect((String) textArray[i]));
            }
        }
        return encoded.toString();
    }

    /**
     * The text is easily decoded by just reflecting the text again so we just call the encode method.
     *
     * @param text The text we want to decode
     * @return The decoded text
     */
    public String decode(String text) {
        return encode(text);
    }

}
