package Techniques;

public class Vigenere {

    /**
     * Writes the codeword under the text. In the background there is a counter that shows at what position of the code
     * we are. It then adds that counter to the StringBuilder which we then convert to String.
     *
     * @param text The text we encode
     * @param code The password
     * @return
     */
    public static String codeIt(String text, String code){
        StringBuilder s=new StringBuilder();
        int lettersAmount=0;
        for(int i=0;i<text.length();i++){
            if('a'<=text.charAt(i)&&text.charAt(i)<='z'||'A'<=text.charAt(i)&&text.charAt(i)<='Z'){
                s.append(code.charAt(lettersAmount%(code.length())));
                lettersAmount++;
            }
        }
        return s.toString();
    }

    /**
     * If a number is negative we add the mod to make it positive again.
     *
     * @param number The negative number
     * @param mod
     * @return The positive number in the same modulo class
     */
    public static int modNegative(int number, int mod){
        while (number <0){
            number+=mod;
        }
        return number;
    }

    /**
     * The big advantage with this encoder compared to the Caesar is that the password can include every character
     * If there appears a letter in the text we want to encode, we have a look at the String we generated with the
     * codeIt method. What we actually do is, we add the value of the codeIt String at a certain position to the
     * to the current letter modulo 26, because the alphabet has the amount of 26.
     * Encode formula: (alphabet position of current letter) + (alphabet position of letter in information String)
     *
     * @param text The text we encode
     * @param code The password
     * @return The encoded text
     */
    public String encode(String text, String code){
        StringBuilder encoded=new StringBuilder();
        String information = codeIt(text, code);
        int count=0;
        for(int i=0;i<text.length();i++) {
            if ('a' <= text.charAt(i) && text.charAt(i) <= 'z' ) {
                encoded.append((char) (modNegative((text.charAt(i)-'a' + Character.toLowerCase(information.charAt(count))-'a') % 26,26) + 'a'));
                count++;
            }else if('A' <= text.charAt(i) && text.charAt(i) <= 'Z'){
                encoded.append((char) (modNegative((text.charAt(i)-'A' + Character.toUpperCase(information.charAt(count))-'A') % 26,26) + 'A'));
                count++;

            //if it's not a letter just add it
            } else {
                encoded.append(text.charAt(i));
            }
        }
        return encoded.toString();
    }

    /**
     * Reverses what we did in the encode method. We can just  transform the formula we used in the encode method
     * Decode formula:  (position of current letter in alphabet) - (alphabet position of letter in information String)
     *
     * @param text The text we want to decode
     * @param code The password
     * @return The decoded text
     */
    public String decode(String text, String code){
        StringBuilder s=new StringBuilder();
        String information = codeIt(text, code);
        int count=0;
        for(int i=0;i<text.length();i++) {
            if ('a' <= text.charAt(i) && text.charAt(i) <= 'z' ) {
                s.append((char) (modNegative((text.charAt(i)-'a' - Character.toLowerCase(information.charAt(count))+'a') % 26,26) + 'a'));
                count++;
            }else if('A' <= text.charAt(i) && text.charAt(i) <= 'Z'){
                s.append((char) (modNegative((text.charAt(i)-'A' - Character.toUpperCase(information.charAt(count))+'A') % 26 ,26)+ 'A'));
                count++;
            } else {
                s.append(text.charAt(i));
            }
        }
        return s.toString();
    }

}
