package Techniques;

import java.util.Arrays;

public class Caesar {

    /**
     * Encodes a text with a password which is only out of letters and every letter that
     * appears in the password must only appear once, otherwise it won't work.
     *
     * @param text The text we want to encode
     * @param code That's the password
     * @return The encoded text
     */
    public String encode(String text, String code) throws MyException{
        //if the password doesn't only consist of letters throw exception
        boolean[] chars=new boolean[26];
        for(int i=0;i<code.length();i++){
          char current=code.charAt(i);
          if((current<'a'||current>'z')&&(current<'A'||current>'Z')|| chars[Character.toLowerCase(current)-'a']){
                throw new MyException("Password should only consist of letters without duplicates.\nPasswort darf nur aus Buchstaben ohne Duplikate bestehen.");
          }
          chars[Character.toLowerCase(current)-'a']=true;
        }

        //get two code array one with lower and one with upper characters
        char[] codeArrayU= getArray(code);
        char[] codeArrayL=codeArrayU.clone();
        String h=new String(codeArrayL);
        String k=h.toLowerCase();
        codeArrayL=k.toCharArray();

        StringBuilder encoded=new StringBuilder();

        //replace all letters with their corresponding replacement
        for(int i=0; i<text.length();i++){
            if('A'<=text.charAt(i)&&text.charAt(i)<='Z'){
                encoded.append(codeArrayU[text.charAt(i)-'A']);
            } else if('a'<=text.charAt(i)&&text.charAt(i)<='z'){
                encoded.append(codeArrayL[text.charAt(i)-'a']);
            }else{
                encoded.append(text.charAt(i));
            }
        }
        return encoded.toString();
    }

    /**
     * Reverse the process we did with the encode method.
     *
     * @param text The text we want to decode
     * @param code The password
     * @return The encoded text
     */
    public String decode(String text, String code) {
        char[] codeArrayU = getArray(code);
        char[] codeArrayL = codeArrayU.clone();
        String h = new String(codeArrayL);
        String k = h.toLowerCase();
        codeArrayL = k.toCharArray();

        StringBuilder encoded = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            //reverse what we did in the encode method
            if ('a' <= text.charAt(i) && text.charAt(i) <= 'z' || 'A' <= text.charAt(i) && text.charAt(i) <= 'Z') {
                int l = 0;
                while (l < codeArrayU.length && text.charAt(i) != codeArrayU[l] && text.charAt(i) != codeArrayL[l]) {
                    l++;
                }
                if (l < codeArrayU.length && Character.isUpperCase(text.charAt(i))) {
                    encoded.append((char) ('A' + l));
                } else if (l < codeArrayU.length && Character.isLowerCase(text.charAt(i))) {
                    encoded.append((char) ('a' + l));
                } else {
                    encoded.append(text.charAt(i));
                }

            }else{
                encoded.append(text.charAt(i));
            }
        }
        return encoded.toString();
    }

    /**
     * A method making it easier to encode and decode the text. With it we can easily find out what letter replaces
     * another letter in the encoding text.
     *
     * @param code The password
     * @return An encoding matrix that where every letter is represented by a different letter depending on the password
     */
    public static char[] getArray(String code) {
        code = code.toUpperCase();
        char[] codeArray=new char[26];
        int l=65;
        for (int i=0; i<codeArray.length; i++) {
            //we write the code into the new array first
            if (i<code.length()) {
                codeArray[i]=code.charAt(i);
            //after we wrote the code into the array we write the missing letters into the array
            } else {
                //find a letter that hasn't been inserted into the new array
                while (code.indexOf((char)l)!=-1) {
                    l++;
                }
                codeArray[i]=(char)l;
                l++;
            }
        }
        return codeArray;
    }

    public static class MyException extends IllegalArgumentException{
        MyException(String message){
            super(message);
        }
    }

}
