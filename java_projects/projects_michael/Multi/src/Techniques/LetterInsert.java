package Techniques;

public class LetterInsert {

    /**
     * Inserts a certain amount of characters between every letter.
     *
     * @param text The text we want to encode
     * @param times How many letters do we want to insert between every character
     * @return The encoded text
     */
    public String encode(String text, int times) {
        StringBuilder encoded = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            encoded.append(text.charAt(i));
            int f = 1;
            //insert certain amount of letters
            while (f < times) {
                //decide if upper or lower character and add a random chosen character
                if (Math.random() > 0.5) encoded.append((char) (65 + 25 * Math.random()));
                else encoded.append((char) (97 + 25 * Math.random()));
                f++;
            }
        }
        return encoded.toString();
    }

    /**
     * Extracts the letters we inserted with the encode method.
     *
     * @param text The text we want to encode
     * @param times How many letters we inserted between every character
     * @return The decoded text
     */
    public String decode(String text, int times) {
        StringBuilder z = new StringBuilder();
        for (int i = 0; i < text.length(); i+=times) {
            z.append(text.charAt(i));
        }
        return z.toString();
    }

}
