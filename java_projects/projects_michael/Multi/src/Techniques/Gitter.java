package Techniques;

import java.util.ArrayList;

public class Gitter {

    public static char[][] matrix={{'A','B','C','a','b','c'}
                                  ,{'D','E','F','d','e','f'}
                                  ,{'G','H','I','g','h','i'}
                                  ,{'J','K','L','j','k','l'}
                                  ,{'M','N','O','m','n','o'}
                                  ,{'P','Q','R','p','q','r'}
                                  ,{'S','T','U','s','t','u'}
                                  ,{'V','W','X','v','w','x'}
                                  ,{'Y','Z','A','y','z','a'}};

    /**
     * Filters out the words in the text because with the encode method we will only encode words.
     *
     * @param text The text we want to encode
     * @return A list with the words we have in the text
     */
    public static Object[] textAsArray(String text) {
        ArrayList<String> words = new ArrayList<String>();
        int i = 0;
        int last = text.length() - 1;
        while (i <= last) {
            int startPoint = i;
            while (i <= last && !('a'<=text.charAt(i)&&text.charAt(i)<='z'||'A'<=text.charAt(i)&&text.charAt(i)<='Z')) i++;
            if (i > startPoint) words.add(text.substring(startPoint, i));
            int startPoint2 = i;
            while (i <= last && ('a'<=text.charAt(i)&&text.charAt(i)<='z'||'A'<=text.charAt(i)&&text.charAt(i)<='Z')) i++;
            if (i > startPoint2) words.add(text.substring(startPoint2, i));
        }
        return words.toArray();
    }

    /**
     * With the help of the matrix we defined we encode the letters of every word. Before every word we append "**"
     * to know where a word starts for the decode. Letters are represented with the row and column number in the matrix.
     * After every number we append a '/' to know for the decode that the number is over.
     *
     * @param text The text we want to encode
     * @return The encoded text
     */
    public String encode(String text){
        StringBuilder encoded = new StringBuilder();
        Object[] textArray =  textAsArray(text);
        for(int i=0; i<textArray.length;i++){

            //take a word and encode it
            String current=(String)textArray[i];
            if('a'<=current.charAt(0)&&current.charAt(0)<='z'||'A'<=current.charAt(0)&&current.charAt(0)<='Z'){
                encoded.append("**");

                //find the position of every letter in the matrix and encode
                for(int currPos=0;currPos<current.length();currPos++) {
                    if(currPos!=0) encoded.append("/");
                    int row = 0, column = 0;
                    outer:
                    for (row = 0; column < matrix.length; row++) {
                        for (column = 0; column < matrix[0].length; column++) {
                            if (matrix[row][column] == current.charAt(currPos)) {
                                break outer;
                            }
                        }
                    }
                    encoded.append(row+""+column);

                }
                encoded.append("**");
            }else{
                encoded.append(current);
            }
        }
        return encoded.toString();
    }

    /**
     * We just reverses the things we did with the encode method. If there is a "**" we know that we have to decode a
     * word. All other characters will stay the same.
     *
     * @param text The text we want to encode
     * @return The encoded text
     */
    public String decode (String text){
        StringBuilder decoded = new StringBuilder();
        for(int i=0;i<text.length();){
            if(i!=0&&text.charAt(i-1)=='*'&&text.charAt(i)=='*'){
                decoded.deleteCharAt(decoded.length()-1);
                int j=i+1;
                while(text.charAt(j)!='*'){
                    int row=text.charAt(j)-'0';
                    int column=text.charAt(j+1)-'0';
                    decoded.append(matrix[row][column]);
                    j+=3;
                }
                i=j+1;
            }else{
                decoded.append(text.charAt(i));
                i++;
            }
        }
        return decoded.toString();
    }

}
