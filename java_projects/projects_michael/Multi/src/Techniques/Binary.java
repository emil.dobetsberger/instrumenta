package Techniques;

public class Binary {

    /**
     * Represents every Character that is smaller than 256 in its 8Bit representation.
     *
     * @param text The text we want to encode
     * @return The encoded text
     */
    public String encode(String text){
        StringBuilder encoded=new StringBuilder();
        for(int i=0; i<text.length();i++){
            if(0<=text.charAt(i)&&text.charAt(i)<=255) {
                String binaryChar = String.format("%8s", Integer.toBinaryString(text.charAt(i))).replaceAll(" ", "0");
                encoded.append(binaryChar);
           }
        }
        return encoded.toString();
    }

    /**
     * Converts the bit representations back into characters.
     *
     * @param text The text we want to decode
     * @return The decoded text
     */
    public String decode(String text){
        StringBuilder decoded=new StringBuilder();
        for(int i=8; i<=text.length();i+=8){
            String binaryChar = text.substring(i-8, i);
                decoded.append((char)Integer.parseInt(binaryChar, 2));
        }
        return decoded.toString();
    }

}
