package Techniques;

public class Fence {

    /**
     * Splits the text into to parts. The characters that have an even position will be in the first part, the
     * letters that have an odd position will be in the second.
     *
     * @param text The text we want to encode
     * @return The encoded text
     */
    public String encode(String text){
        StringBuilder encoded=new StringBuilder();
        for (int i=0; i<text.length();i+=2){
            encoded.append(text.charAt(i));
        }
        for (int i=1; i<text.length();i+=2){
            encoded.append(text.charAt(i));
        }
        return encoded.toString();
    }

    /**
     * Just we reverses what we did in the encode method. Always reads in one letter from the even block and one from
     * the odd block.
     *
     * @param text The text we decode
     * @return The decoded text
     */
    public String decode(String text){
        StringBuilder decoded=new StringBuilder();
        if(text.length()%2==0){
            for(int i=0;i<text.length()/2;i++){
                decoded.append(text.charAt(i)+""+text.charAt(i+text.length()/2));
            }
        }else{
            for(int i=0;i<text.length()/2+1;i++){
                decoded.append(text.charAt(i));
                if(i!=text.length()/2){
                    decoded.append(text.charAt(i+text.length()/2+1));
                }
            }
        }
        return decoded.toString();
    }

}
