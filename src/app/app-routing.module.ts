import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/components';

import { HomeRoutingModule } from './home/home-routing.module';

import { RngComponent } from './rng/rng.component';
import { ImageFilterComponent } from './image-filter/image-filter.component';
import { CryptoComponent } from './crypto/crypto.component';
import { CharCountComponent } from './char-count/char-count.component'
import { EeaComponent } from './eea/eea.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: 'rng', component: RngComponent },
  { path: 'crypto', component: CryptoComponent },
  { path: 'image-filter', component: ImageFilterComponent },
  { path: 'char-count', component: CharCountComponent },
  { path: 'eea', component: EeaComponent},
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }),
    HomeRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
