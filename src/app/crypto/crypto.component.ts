import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ipcRenderer } from 'electron';
import { ElectronService } from '../core/services/electron/electron.service'

interface CryptoAlgorithm {
  value: string;
  viewValue: string;
  keyType: number; // 0: none 1: number 2: string
  command : string;
}

@Component({
  selector: 'app-crypto',
  host: { class: 'outlet' },
  templateUrl: './crypto.component.html',
  styleUrls: ['./crypto.component.scss']
})
export class CryptoComponent implements OnInit {

  mode: string = "enc";
  key: number = 0;
  password: string;
  validPw: boolean = false;

  inputStr: string = "";
  outputStr: string = ""
  jarSignal : string = "runCrypto";

  algorithms: CryptoAlgorithm[] = [
    {value: 'binary-0', viewValue: 'PAGES.CRYPTO.BINARY', keyType: 0, command: "Binary"},
    {value: 'vigenere-7', viewValue: 'PAGES.CRYPTO.VIGENERE', keyType: 2, command: "Vigenere"},
    {value: 'caesar-1', viewValue: 'PAGES.CRYPTO.CAESAR', keyType: 2, command: "Caesar"},
    {value: 'rot-6', viewValue: 'PAGES.CRYPTO.ROT', keyType: 1, command: "Rot"},
    {value: 'fence-2', viewValue: 'PAGES.CRYPTO.FENCE', keyType: 0, command: "Fence"},
    {value: 'gitter-3', viewValue: 'PAGES.CRYPTO.GITTER', keyType: 0, command: "Gitter"},
    {value: 'letter-insert-4', viewValue: 'PAGES.CRYPTO.LETTERINSERT', keyType: 1, command: "LetterInsert"},
    {value: 'reflect-5', viewValue: 'PAGES.CRYPTO.REFLECT', keyType: 0, command: "Reflect"}
  ];

  algoDict = {}; // initialized in constructor
  selectedAlgo = this.algorithms[0].value;

  constructor(private router: Router, private electron: ElectronService, private changeDetector: ChangeDetectorRef) { 
    if(this.electron.ipcRenderer) {
      this.electron.ipcRenderer.removeAllListeners('output'); // output event
      this.electron.ipcRenderer.on('output', (event, arg) => {
        if (arg.length > 0) {
          console.log("argument received : " + arg)
          this.outputStr = atob(arg); // convert back from base64
          changeDetector.detectChanges(); // update DOM
        }
      })
    }

    this.algorithms.forEach(elem => this.algoDict[elem.value] = elem) // populate dictionary
  }

  ngOnInit(): void { }

  updatePassword(password: string) { // check for forbidden characters
    this.password = password;
    var letters = /^[A-Za-z]+$/;
    if(password.match(letters)) {
      this.validPw = true;
    } else {
      this.validPw = false;
    }
  };

  runJar(): void {
    if(this.inputStr) {
      if(this.electron.ipcRenderer) {
        var algo = this.algoDict[this.selectedAlgo];
  
        var infoStr = algo.command;
        infoStr = infoStr.concat(this.mode == "enc" ? "/encode" : "/decode"); // infostring for procedure
  
        if(algo.keyType == 1) {
          var keyNum = Number.parseInt(""+this.key);
          if(Number.isNaN(keyNum)) {
            keyNum = 0;
          }
          
          infoStr = infoStr.concat('/').concat(keyNum.toString()); // add key to infostring
        }
        else if (algo.keyType == 2) {
          if (!this.validPw) {
            console.log("error: password is not valid");
            return;
          }
          infoStr = infoStr.concat('/').concat(this.password); // add password to infostring
        }
  
        var base64 = btoa(this.inputStr); // convert input to base64 to prevent any transmission erros
        var varargs = '"' + (infoStr) + '" "' + base64;
       
        console.log('sending signal [' + this.jarSignal + '] with argument: ' +  varargs);
        this.electron.ipcRenderer.send(this.jarSignal, varargs); // send event with args for main process to react to
      } else {
        console.error("Can't execute scripts in browser mode!");
      }
    }
    
  }

}
