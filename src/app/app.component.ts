import { Component } from '@angular/core';
import { ElectronService } from './core/services';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  expanded : Boolean = true;
  cooldown : Boolean = false;
  overflowed : Boolean = true;
  timeout : NodeJS.Timeout;

  constructor(
    private electronService: ElectronService,
    private translate: TranslateService
  ) {
    this.translate.use('de');
    this.translate.use('es');
    this.translate.use('fr');
    this.translate.use('en');
    this.translate.setDefaultLang('en');
    console.log('AppConfig', AppConfig);
    console.log('supported languages:');
    console.log(this.translate.getLangs());

    if (electronService.isElectron) {
      console.log(process.env);
      console.log('Run in electron');
      console.log('Electron ipcRenderer', this.electronService.ipcRenderer);
      console.log('NodeJS childProcess', this.electronService.childProcess);
    } else {
      console.log('Run in browser');
    }
  }

  toggleToolbar() {
    if(!this.cooldown) {
      this.expanded = !this.expanded;
      
      if(this.expanded) {
        this.cooldown = true;
        this.timeout = setTimeout(() => {
          this.overflowed = true;
          this.cooldown = false;
        }, 350);

      } else {
        this.overflowed = false;
        if(this.timeout) clearTimeout(this.timeout);
      }
    }
  }

  getLangs() {
    return this.translate.getLangs();
  }
  changeLang(value) {
    this.translate.use(""+value)
  }
  getCurrentLang() {
    return this.translate.currentLang;
  }
}
