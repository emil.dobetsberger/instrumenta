import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rng',
  host: { class: 'outlet' },
  templateUrl: './rng.component.html',
  styleUrls: ['./rng.component.scss']
})
export class RngComponent implements OnInit {

  default = "./assets/rng/0 - Blue.png";
  sources = [this.default, this.default, this.default, this.default, this.default, this.default];
  colors = ["Blue", "Green", "Red", "Yellow"];
  numbers = 2;
  constructor() { }

  ngOnInit(): void {
  }

  getRandom(): void {
    for(var i = 0; i < 6; i++) {
      if (i <this.numbers) {
        var color = Math.floor(Math.random()*4); //get random in [0,1,2,3]
        var colorStr = this.colors[color];
        var digit = Math.floor(Math.random()*10);
        this.sources[i] = "./assets/rng/" + digit + " - " + colorStr +".png";
      } else {
        this.sources[i] = this.default;
      }
    }
  }

}
