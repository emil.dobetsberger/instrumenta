import { ChangeDetectorRef, Component, Output, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ElectronService } from '../core/services/electron/electron.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

interface Filter {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-image-filter',
  host: { class: 'outlet' },
  templateUrl: './image-filter.component.html',
  styleUrls: ['./image-filter.component.scss']
})
export class ImageFilterComponent implements OnInit {

  // fields for executable
  jarSignal : string = "runImageFilter";

  // fields for image data
  source : string; // raw data
  sourcePath : string; // path to file to pass to .jar file
  destinationPath : string;
  version : number = 0;
  filteredImgSrc : SafeResourceUrl;
  loading: boolean = false;
  threshold = 128;

  filters: Filter[] = [
    {value: 'grayscaleFilter', viewValue: 'PAGES.IMGFILTER.GRAYSCALE'},
    {value: 'sepiaFilter', viewValue: 'PAGES.IMGFILTER.SEPIA'},
    {value: 'thresholdFilter', viewValue: 'PAGES.IMGFILTER.THRESHOLD'},
    {value: 'edgeFilter', viewValue: 'PAGES.IMGFILTER.EDGE'},
    {value: 'blurFilter', viewValue: 'PAGES.IMGFILTER.BLUR'}
  ];

  filterDict = {}; // initialized in constructor

  selected = this.filters[0].value;

  @Output() onChange: EventEmitter<File> = new EventEmitter<File>();

  constructor(private router: Router, private electron: ElectronService, private changeDetector: ChangeDetectorRef, private domSanitizer: DomSanitizer) { 
    if(this.electron.ipcRenderer) {
      this.electron.ipcRenderer.removeAllListeners('output');
      this.electron.ipcRenderer.on('output', (event, arg) => { // output event
        if (arg.length > 0) {
          
          this.filteredImgSrc = domSanitizer.bypassSecurityTrustUrl(`load-img://${this.destinationPath}`);
          this.loading = false;
          console.log("argument received : " + this.filteredImgSrc);
          changeDetector.detectChanges(); // update DOM
        }
      })
    }

    this.filters.forEach(element => { // populate dictionary
      this.filterDict[element.value] = element;
    });
  }

  ngOnInit(): void {
  }
  
  updateSource($event: Event) {
    // access file
    this.projectImage($event.target['files'][0]);
  }

  projectImage(file: File) {
    if(file) {
      let reader = new FileReader;
      reader.onload = (e: any) => {
          // Simply set e.target.result as our <img> src in the layout
          if(this.source !== e.target.result) {
            this.version = 0;
          }
          this.source = e.target.result;
          this.onChange.emit(file);
      };
      // This will process our file and get it's attributes/data
      reader.readAsDataURL(file);

      this.sourcePath = file.path;
    }
  }

  runJar(): void {

    // only if image has been selected
    if (this.sourcePath != null && !this.loading) {
      this.loading = true;
      this.filteredImgSrc = null;
      // create destination path
      do{
      var iExt = this.sourcePath.lastIndexOf('.');
      var extensionlessPath = this.sourcePath.substr(0, iExt);
      var extension = this.sourcePath.substr(iExt, this.sourcePath.length - iExt);

      this.destinationPath = extensionlessPath.concat("_filtered_").concat(this.version.toString()).concat(extension);

      this.version++
      // check if file already exists
      } while(this.electron.fs.existsSync(this.destinationPath));
      
      // run jar

      if(this.electron.ipcRenderer) {
        var varargs =  '"' + this.selected + '" "' + this.sourcePath + '" "' + this.destinationPath + '" "' + extension.replace(".","") + '"';
        if(this.selected == "thresholdFilter") { varargs = varargs + ' "' + this.threshold + '"'}
        console.log('sending signal [' + this.jarSignal + '] with argument: ' +  varargs);
        this.electron.ipcRenderer.send(this.jarSignal, varargs);
        
      } else {
        console.error("Can't execute scripts in browser mode!");
      }

    }

    

  }

}
