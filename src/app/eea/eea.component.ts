import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ElectronService } from '../core/services/electron/electron.service'

@Component({
  selector: 'app-eea',
  host: { class: 'outlet' },
  templateUrl: './eea.component.html',
  styleUrls: ['./eea.component.scss']
})
export class EeaComponent implements OnInit {

  num1: number = 0;
  num2: number = 0;
  
  outputStr: string = ""
  jarSignal : string = "runEEA";


  constructor(private router: Router, private electron: ElectronService, private changeDetector: ChangeDetectorRef) { 
    if(this.electron.ipcRenderer) {
      this.electron.ipcRenderer.removeAllListeners('output');
      this.electron.ipcRenderer.on('output', (event, arg) => { // on output event
        if (arg.length > 0) {
          console.log("argument received : " + arg)
          this.outputStr = arg;
          changeDetector.detectChanges(); // update DOM
        }
      })
    }
  }

  ngOnInit(): void {
  }

  runJar(): void {
    if(this.num1 > 0 && this.num2 > 0) {
      if(this.electron.ipcRenderer) {

        var varargs = '"' + this.num1 + '" "'+ this.num2 + '"'

        console.log('sending signal [' + this.jarSignal + '] with argument: ' +  varargs);
        this.electron.ipcRenderer.send(this.jarSignal, varargs); // send event with args for main process to react to
      } else {
        console.error("Can't execute scripts in browser mode!");
      }
    }
  }
}
