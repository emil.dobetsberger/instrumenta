import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ElectronService } from '../core/services/electron/electron.service'

@Component({
  selector: 'app-char-count',
  host: { class: 'outlet' },
  templateUrl: './char-count.component.html',
  styleUrls: ['./char-count.component.scss']
})
export class CharCountComponent implements OnInit {
  inputStr: string = "";
  outputStr: string = ""
  jarSignal : string = "runCharCount";

  selectedMutation = '0';

  igReturnNewLine : boolean = false;
  igSpace : boolean = false;
  igCase : boolean = false;
  igSpecial : boolean = false;

  constructor(private router: Router, private electron: ElectronService, private changeDetector: ChangeDetectorRef) { 
    if(this.electron.ipcRenderer) {
      this.electron.ipcRenderer.removeAllListeners('output');
      this.electron.ipcRenderer.on('output', (event, arg) => {
        if (arg.length > 0) {
          console.log("argument received : " + arg)
          this.outputStr = arg.replace(/[\n\r]/, ''); // remove an eventual newline at the beginning
          changeDetector.detectChanges(); // update DOM
        }
      })
    }
  }

  ngOnInit(): void {
  }

  runJar() {
    if(this.inputStr) {
      if(this.electron.ipcRenderer) {
          
        var digiargs = ' "' + (this.igReturnNewLine ? 1 : 0) + '"' +
                      ' "0"' + // legacy parameter
                      ' "' + (this.igSpace ? 1 : 0) + '"' +
                      ' "' + (this.igCase ? 1 : 0) + '"' +
                      ' "' + (this.igSpecial ? 1 : 0) + '"' +
                      ' "' + (this.selectedMutation) + '"';
  
        // Replace one backslash with two backslashes
        // and replace returns with '\n'
        // replace " with ' to prevent premature argument termination
        var varargs = '"' + this.inputStr.replace("\\","\\\\").replace(/[\n\r]/g, '\\n').replace(/"/g, "'") + '"' + digiargs;
  
        console.log('sending signal [' + this.jarSignal + '] with argument: ' +  varargs);
        this.electron.ipcRenderer.send(this.jarSignal, varargs); // send event with args for main process to react to
      } else {
        console.error("Can't execute scripts in browser mode!");
      }
    }
  }

}
